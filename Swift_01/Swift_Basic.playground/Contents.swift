//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//1. Basic Operator
let contentHeight = 40
let hasHeader = true
let rowHeight = contentHeight + (hasHeader ? 50 : 20)

//2. Nil-coalesing Operator
let defaultColorName = "red"
var userDefinedColorName: String?   // defaults to nil

var colorNameToUse = userDefinedColorName ?? defaultColorName

userDefinedColorName = "green"
colorNameToUse = userDefinedColorName ?? defaultColorName

//3. Range Operators

//Closed range operator

for index in 1...5 {
    print("\(index) time 5 is \(index*5)")
    
}
//Half-Open range operator

let name = ["Anna","Alex","Brian","Jack"]

let count = name.count

for i in 0..<count {
    print("Person \(i+1) is call \(name[i])")
}


//4. Strings and Characters
//4.1 String literals

let someString = "Some string literal value"

// Initializing an Empty string

var emptyString = ""

var anotherEmptyString = String ()    // initializer syntax

if emptyString.isEmpty {
    print("Nothing to see here")
}

//String mutability 
var variableString = "Horse"
variableString  += "and carriage"

let constantString = "HighlandCoffe"
// constantString += "and other highlandCoffe"

// String are value Type
for character in "Dog!@".characters {
    print(character)
}

//Character

let catCharacters : [Character] = ["C", "a", "t", "!", "@"]
let catString  = String(catCharacters)

//Concatenating strings and characters
let string1 = "hello"
let string2 = "there"
var welcome = string1 + string2
// append string

var instruction = "look over"
instruction += string2
let exclamationMark : Character = "!"
welcome.append(exclamationMark)

//String interpolation
let multiplier = 3
//let message = "\(multiplier) times

// Inserting and removing

welcome.insert("!", at: welcome.endIndex)

welcome.insert(contentsOf: "hello".characters, at: welcome.index(before: welcome.endIndex))
welcome.remove(at: welcome.index(before: welcome.endIndex))

//Comparing strings
// Prefix and suffix equality
let romeoAndJuliet = ["Act 1 Scense 1: Verona, A pu",
                      "Act 1 Scense 2: Cap",
                      "Act 1 ABC",
                      "Act 2 Test",
                      "Act 2 Test2"]

var act1SceneCount = 0
for scene in romeoAndJuliet {
    if scene.hasPrefix("Act 1") {
        act1SceneCount += 1
        
    }
}

print("There are \(act1SceneCount) scene in Act 1")

