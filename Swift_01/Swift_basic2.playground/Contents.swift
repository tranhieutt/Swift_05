//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//Array type
var someIns = [Int] ()

someIns.append(3);

someIns = []

//Create array

var threeDoubles = Array(repeating: 1, count: 3)
var anotherThreeDouble = Array(repeating: 4, count: 3)

var sevenArray = threeDoubles + anotherThreeDouble

//Array literal

var shoppingList : [String] = ["Egg","Milk"]
shoppingList.append(String(1))

shoppingList[2] = String(3)

shoppingList.append("Test")

shoppingList.insert(String(6), at: 4)



// Iterating Over an Array
for (index, value) in shoppingList.enumerated() {
    print("Item \(index + 1): \(value)")
}
// Set
var letters = Set <Character>()

letters.insert("a")

letters = []

var favoriteGenres: Set<String> = ["Rock","Classical","Hip hop"]
// Accessing and modifying a Set

favoriteGenres.insert("Country")

for genre in favoriteGenres {
    print("\(genre)")
}

// Performing Set operation
let oddDigit: Set = [1,3,5,7,9]
let evenDigit: Set = [0,2,4,6,8]
let singleDigitPrimeNumbers : Set = [2,3,5,7]

oddDigit.union(evenDigit).sorted(){
    $0 > $1
}

oddDigit.intersection(evenDigit).sorted() {
    $0 < $1
}

oddDigit.subtracting(singleDigitPrimeNumbers).sorted() {
    $0 > $1
}
oddDigit.symmetricDifference(singleDigitPrimeNumbers).sorted() {
    $0 < $1
}

//Dictionary
//Creating an empty dictionary
var nameOfInteger = [Int: String]()

nameOfInteger[16] = "sixteen"
//nameOfInteger = [:]
var airports : [String: String] = ["YYZ":"Toronto","DUB":"Dublin"]

//Accessing and modify a dictionary
if airports.isEmpty {
    
}

airports["LHR"] = "London Heathrow"

print((airports))
// UpdateValue return old value of key. and then set new value for key
airports.updateValue("London Underground", forKey: "LHR")

if let oldValue = airports.updateValue("test", forKey: "YYZ") {
    print("the old value \(oldValue)")
}
// Unwrap value
if let airPortName = airports["LHR"] {
    print(airPortName)
}
if let removeValue = airports.removeValue(forKey: "LHR") {
    print(" The removed airport is \(removeValue)")
}
airports["LRH"]

// Iterating over a Dictionary 
for (airportCode, airportName) in airports {
    print("\(airportCode): \(airportName)")
}

for airportName in airports.keys {
    print("\(airportName)")
}
