//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//Instance method
class Counter {
    var count = 0
    func increment () {
        count += 1
    }
    func increment(by ammout:Int) {
     count += ammout
    }
    
    func reset(){
        count = 0
    }
}

let counter = Counter()
counter.increment()

counter.count

counter.increment(by: 5)


counter.reset()

counter.count = 7

//Modify value types from within instance methods
//Structures and enumeration are value types. It mean, the properties of a value type can not be modified from within its
// instance methods
//To modify the properties of structure or enumeration within a particular method, use mutating behavior for that method

struct Point {
    var x = 0.0, y = 0.0
     mutating func moveBy(x deltaX:Double, y deltaY:Double) {
        x += deltaX
        y += deltaY
    }
}


var somePoint = Point(x: 1.0, y: 1.0)
somePoint.moveBy(x: 2.0, y: 3.0)
//Assigning to self within a mutating method

struct OtherPoint {
    var x = 0.0, y = 0.0
    mutating func   moveBy(x deltaX: Double, y deltaY: Double) {
        self = OtherPoint(x: x+deltaX, y: y+deltaY)
    }
}


//Same enumeration
enum TriStateSwitch {
    case off, low, high
    
    mutating func next () {
        switch self {
        case .off:
            self = .low
        case .low:
            self = .high
        case .high:
            self = .off
        }
    }
}

var overLight = TriStateSwitch.low

overLight.next()
overLight.next()


//Type methods
//Instance methods, are methods that are called on an instance of a particular

//Type method :indicate by writing the static keyword before the method's func
//or use the class keyword to allow subclasses to override the superclass's
struct LevelTracker {
    static var highestUnlockedLevel = 1
    var currentLevel = 1
    static func unlock(_ level : Int) {
        if level > highestUnlockedLevel {
            highestUnlockedLevel = level
        }
    }
    
     static func isUnlocked (_ level : Int) -> Bool {
            return level <= highestUnlockedLevel
        }

    @discardableResult
    mutating func advance (to level: Int) -> Bool {
        if LevelTracker.isUnlocked(level) {
            currentLevel = level
            return true
        } else  {
            return false
        }
    }
    
    }


class Player {
    var tracker = LevelTracker ()
    let playerName : String
    func complete(level: Int) {
        LevelTracker.unlock(level + 1)
        tracker.advance(to: level + 1)
    }
    init(name: String) {
        playerName = name
    }
}

var player = Player (name:  "Argy bug")
player.complete(level: 1)
print("highest unlocked level is now \(LevelTracker.highestUnlockedLevel)")

