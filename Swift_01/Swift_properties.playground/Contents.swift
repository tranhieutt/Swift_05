//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//Stored propertises: can be either variable stored properties 

struct FixedLengthRange {
    var firstValue: Int
    let lenght: Int
}


var rangeOfThreeItems = FixedLengthRange (firstValue: 0, lenght: 3)

print(rangeOfThreeItems.firstValue, rangeOfThreeItems.lenght)

//Stored propertises of Constant struture instances

let otherRangeOfThreeItems = FixedLengthRange (firstValue: 1, lenght: 5)

//otherRangeOfThreeItems.firstValue = 7
//Error 


//Lazy stored propertise
// A lazy stored property is a property whose initial value is not calculated until the first time it is used

// The example below used a lazy stored property to avoid unnecessary initialization of complex class
class DataImporter {
    
    var filename = "data.txt"
    
}

class DataManager {
    lazy var importer = DataImporter ()
    var data = [String] ()
    
}

let manager = DataManager ()
manager.data.append("Some data")
manager.data.append("Some more data")


//Because it is marked with the lazy modifier the DataImporter instance for the imported property is only created when the importer
// property is first accessed, such as when its filename property is queried
print(manager.importer.filename)

//Computed properties
//In addition to stored properties, classed, strutures and enummertation can define computed properties, which
// do not actually store a value. Instead, they provide a getter and an optional setter to retrieve and set other
//properties and values indirectly

struct Point {
    var x = 0.0, y = 0.0
}

struct Size {
    var width = 0.0, height = 0.0
    
}

struct Rect {
    var origin = Point ()
    var size = Size ()
    var center : Point {
        get {
            let centerX = origin.x + (size.width/2)
            let centerY = origin.y + (size.height/2)
            return Point(x: centerX, y: centerY)
            
        }
        set (newCenter) {
            origin.x = newCenter.x - (size.width/2)
            origin.y = newCenter.y - (size.height/2)
        }
    }
}

var square = Rect (origin: Point(x:0.0, y: 0.0), size: Size(width: 10.0, height: 10.0))
//when get
let initialSquareCenter = square.center

print(initialSquareCenter)
//when set
square.center = Point (x: 15, y: 15)

print(square.center, square.origin.x, square.origin.y)
//The Rect structure alse provide a computed property called center.


//Read-only computed properties

//Property Observers
//Property observers observe and respond to changes in a property's value. Property observers are called every
//time a property's value is set, even if the new valu is the same as the property's current value
//The option to define either or both of these observers on a property
// willSet is called just before the value is stored
// didSet is called immediately after the new value is stored

class StepCounter {
    var totalStep: Int = 0 {
        willSet(newTotalSteps) {
            print("About to set totalStep to \(newTotalSteps)")
        }
        didSet {
            if totalStep > oldValue {
                print("Added \(totalStep - oldValue) step")
            }
        }
    }
}

let stepCounter = StepCounter ()
stepCounter.totalStep = 200
//when assigned a new value


stepCounter.totalStep = 250
stepCounter.totalStep = 500


//Type Properties
//Type property syntax
struct SomeStructure {
    static var storedTypeProperty = "Some value"
    static var computedTypeProperty: Int {
        return 1
    }
}

enum SomeEnumeration {
    static var storedTypeProperty = "Some value"
    static var computedTypeProperty: Int {
        return 6
    }
}

class SomeClass {
    static var storedTypeProperty = "Some value"
    static var computedTypeProperty: Int {
        return 27
    }
    static var overrideableComputedTypeProperty : Int {
        return 107
    }
}

//Querry and Setting type Properties

print(SomeStructure.storedTypeProperty)

SomeStructure.storedTypeProperty     = "Another Value"

print(SomeStructure.storedTypeProperty)

print(SomeEnumeration.computedTypeProperty)

//Error: SomeEnumeration.computedTypeProperty = 8
print(SomeClass.computedTypeProperty)


