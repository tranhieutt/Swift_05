//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//For-in loops
let numberOfLegs = ["spider":8, "ant":9,"cat":7]

let base = 3
let power = 10
var answer = 1

for _ in 1...power {
    answer *= base
}

let minutes = 60
let minuteInterval = 5
for tickMark in stride(from: 0, to: minutes, by: minuteInterval) {
    print("\(tickMark)")
}


//Switch

//Interval matching
let approximateCount = 62
let countedThings = "moons orbiting Saturn"
let naturalCount: String

switch approximateCount {
case 0:
    naturalCount = "no"
case 1..<5:
    naturalCount = "a few"
case 5..<12:
    naturalCount = "several"
default:
    naturalCount = "many"
}


//Tuples

let somePoint = (1,1)

switch somePoint {
case (0,0):
    print("\(somePoint) is at the origin")
case (_,0):
    print("\(somePoint) is on the x-axis")
    
case (0,_):
    print("\(somePoint) is inside the box")
default:
    
    print("\(somePoint) is outside of the box")
    
}

//Value bindings
let anotherPoint = (2,0)
switch anotherPoint {
case (let x,0):
    print("on the x-axis with an x value of \(x)")
case (0, let y):
    print("on the y-axis with a y value of \(y)")
case let (x,y):
    print("somewhere else at (\(x),(y))")
}

//Compound cases
let someCharacter: Character = "e"
switch someCharacter {
case "a","e","i","o","u":
    print("\(someCharacter) is a vowel")
case "b","c","d":
    print("\(someCharacter) is a consoant")

default:
    print("")
}

//Compound case include value binding
let stillAnotherPoint = (9,0)
switch stillAnotherPoint {
case (let distance,0),(0,let distance):
    
    print("On an axis, \(distance)")
default:
    print("")

}

//Control transfer statements
//1. Continue: stop loop
let puzzleInput = "greate minds think alike"
var puzzleOutput = ""

let charactersToRemove : [Character] = ["a","e","i","o","u"," "]

for character in puzzleInput.characters {
    if charactersToRemove.contains(character) {
     //  continue
    } else {
        puzzleOutput.append(character)
    }
}



print(puzzleOutput)
//Break
//Fallthrough: jumo to next case

let integerToDescribe = 5
var description = "The number \(integerToDescribe) is"
switch integerToDescribe {
case 2,3,5,7,11,13,17,19:
    description += "a prime number, and also"
    fallthrough
case 22,23:
    description += "test"
default:
    description += "an integer"
}

print(description)

//Break

//Early exit
//Guard check nil
func greet(person: [String: String]) {
    guard let name = person["name"] else {
        return
    }
    print("Hello \(name)")
    
    guard let location = person["location"] else {
        return
    }
    
    print("I hope the weather is nice in \(location)")
}

greet(person: ["name":"Jone"])

greet(person: ["name":"Jone","location":"London"])


