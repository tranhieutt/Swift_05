//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//Enummerations defines a common type for a group of related values

enum SomeEnumeration {
    
}

enum CompassPoint {
    case north
    case south
    case east
    case west
}

var directionToHead = CompassPoint.north

directionToHead = .west

//Matching enumeration values with a switch statement

directionToHead = .south

switch directionToHead {
case .north:
    print("North")
case .south:
    print("South")
case .east:
    print("East")
case .west:
    print("West")
}

//Associated values
//Declare enum
enum Barcode {
    case upc(Int, Int, Int, Int)
    case qrCode(String)
}
//Declare var use enum
var productBarCode = Barcode.upc(7, 85909, 51223, 3)

//or
productBarCode = .qrCode("ABCDFFF")

//Only store one of them at any given time
// The difference barcode types can be checked uusing a switch statement
//Extract each associated value as a constant ( use let) or a variable (use var)

switch productBarCode {
case .upc(let numberSystem, let manufactured, let product, let check) :
    print("UPC: \(numberSystem), \(manufactured),\(product), \(check)")
case .qrCode(let productCode):
    print("QR code: \(productCode)")
}
// Raw values
// Raw value can be strings, character, or any of the integer of float
enum ASCIIControlCharacter: Character {
    case tab = "\t"
    case lineFeed = "\n"
    case carriageReturn = "\r"
}

//Implicitly assigned raw values
enum Planet: Int {
    case mercury = 1, venus, earth, mars, jupiter, saturn, uranus, neptune
}

enum CompassPoints: String {
    case north, south, east, west
}

let earthsOrder = Planet.earth.rawValue

let sunsetDirection = CompassPoints.west.rawValue
// Initializing from a raw value
// get value from raw value
let possiblePlanet = Planet(rawValue: 7)

let marsOrder = Planet.mars.rawValue

let posistionToFind = 11
// Create optional
if let somePlanet = Planet(rawValue: posistionToFind) {
    switch somePlanet {
    case .earth:
        print("Mostly harmless")
    default:
        print("Not a safe place for human")
    }
} else {
    print("There isn't a planet at position \(posistionToFind)")
}
//Recursive Enumeration 
//A recursive enummeration is an enumeration that has another instance