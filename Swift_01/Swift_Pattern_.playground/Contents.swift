//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//Pattern matching: 
//Vi du ve pattern matching : su dung switch case
//Switch basics
enum Direction {
    case north, south, east, west
}

// switch between simple enum values
//o, customStringConvertible
extension Direction : CustomStringConvertible {
    var description: String  {
        switch self {
        case .north:
            return "North"
        case .east:
            return "East"
        case .south:
            return "South"
        case .west:
            return "West"
        }
    }
}

//Switch allow match against patterns containing variables, and binding to those variables when it
//matches

//This applies to enum with associated value 
enum Media {
    case book (title: String, author: String, year: Int)
    case movie (title: String, director: String, year:Int)
    case website (url: NSURL, title: String)
}
//let aTitle la mot associated values
extension Media {
    var mediaTilte:String {
        
        switch self {
        case .book(title: let  aTitle , author: _ , year: _):
            return aTitle
        case .movie(title: let aTitle , director: _, year: _):
            return aTitle
        case .website(url: _ , title: let aTitle):
            return aTitle
        }
    }
}

let book = Media.book(title: "Heelow", author: "Ander", year: 2000)

book.mediaTilte
//Ki ty _ cho biet rang, ko can quan tam, la gi cung duoc

let movie = Media.movie(title: "Luar", director: "Anor", year:2016)

movie.mediaTilte
//let: tao ra 1 variable
//Using fixed values
extension Media {
    var isFromJulesVerne: Bool {
        switch self {
        case .book(title: _ , author: "Junles Verne", year: _):
            return true
        case .movie(title: _ , director: "Jone", year: _):
            return true
        default :
            return false
        }
    }
}

let otherBook = Media.book(title: "ABC", author: "Jone", year: 2000)


otherBook.isFromJulesVerne



let otherMovei = Media.movie(title: "Helen", director: "Jone", year: 2500)

otherMovei.isFromJulesVerne

//Generic
extension Media {
    func checkAuthor(_ author:String) -> Bool {
        switch self {
        case .book(title: _ , author: author, year: _):
            return true
        case .movie(title: _ , director: author, year: _ ):
            return true
        default:
            return false
        }
    }
}

otherBook.checkAuthor("Jone")
otherBook.isFromJulesVerne

//Binding multiple pattern at once
//Using tuples without arguemnt labels
extension Media {
    var mediaTitle3 : String {
        switch self {
        case let .book(tupble):
            return tupble.title
        case let .movie(tupble):
            return tupble.title
        case let .website(tupble):
            return tupble.title
        }
    }
}

let otherMedia = Media.movie(title: "Hellen", director: "Trust", year: 2333)
otherMedia.mediaTitle3


//Using Where

//Pattern matching allow compare two enums
extension Media {
    var publishedAfter1930: Bool {
        switch self {
        case let .book(_, _, year) where year > 1930:
            return true
        case let .movie(_, _, year) where year > 1930:
            return true
        case .website:
            return true // same as "case .website(_)" but we ignore the associated tuple value
        default:
            return false
        }
    }
}

otherMedia.publishedAfter1930




