//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//Defining and Calling function

func greet(person: String) -> String {
    let greeting = "hello" + person
    return greeting
}

greet(person: "hieutt14")


//Function parameter and return values

func greet(person: String, alreadyGreeted:Bool) -> String {
    if alreadyGreeted {
        return greet(person: person)
    } else {
        return "hello"
    }
}

print(greet(person: "hieutt14", alreadyGreeted: true))


// Return value of a function can be ignored when it called
func printAndCound (string: String) ->Int {
    print(string)
    return string.characters.count
}

func printWithoutCounting (string: String) {
    let _ = printAndCound(string: string)
}


printWithoutCounting(string: "hello world")

// Function with multiple return values
//func minMax (array: [Int]) -> (min: Int, max: Int){
//    var currentMin = array[0]
//    var currentMax = array[0]
//    
//    
//    for value in array[1..<array.count] {
//        if value < currentMin   {
//            currentMin = value
//        } else if value > currentMax {
//            currentMax = value
//        }
//    }
//    return (currentMin, currentMax)
//}
//
//let bounds = minMax(array: [8, 9, 2, 4,5 ,7])

//print("\(bounds.min) and \(bounds.max)")

//Optional tuple return type

func minMax (array: [Int]) -> (min: Int, max: Int)?{
    var currentMin = array[0]
    var currentMax = array[0]
    
    
    for value in array[1..<array.count] {
        if value < currentMin   {
            currentMin = value
        } else if value > currentMax {
            currentMax = value
        }
    }
    return (currentMin, currentMax)
}

//Use optional binding 

if let bounds = minMax (array: [3,4,6,7,8,9]) {
    print("\(bounds.min) and \(bounds.max)")
}

//Ommitting argument labels
//Bypass label of the first argument

func someFunction(_ firstParam: Int, secondParam: Int) {
    
}

someFunction(1, secondParam: 2)

//Default parameter values

func someOtherFunction (paramWithoutDefault: Int, paramDefaul: Int = 12) {
    print("\(paramWithoutDefault) and \(paramDefaul)")
}
someOtherFunction(paramWithoutDefault: 3, paramDefaul: 47)
someOtherFunction(paramWithoutDefault: 5)

//Variadic Parammeters
//Accepts zero or more values of specified type
func arithmeticMean (_ numbers: Double...) -> Double {
    var total: Double = 0
    for number in numbers {
        total += number
    }
    return total/Double(numbers.count)
}

arithmeticMean(1,3,4,5,6)


// In - out parameter
// Function type
func addTwoInts (_ a: Int, _ b: Int) -> Int {
    return a + b
}

var mathFunction :(Int, Int) -> Int = addTwoInts

//Function type as paramenter types
//Use a funciton type (Int, Int) -> Int as a parameter type for another function 

func printMathResult (_ mathFunction : (Int, Int) -> Int, _ a: Int, _ b: Int) {
    print("Result \(mathFunction(a,b))")
}

printMathResult(addTwoInts, 3, 5)

// Function types as return types
//func stepForward (_ input: Int) -> Int {
//    return input + 1
//}
//func stepBackward (_ input: Int) -> Int {
//    return input - 1
//}
//
//// Return type (Int) -> Int 
//
//func chooseStepFunction (backward: Bool) -> (Int) -> Int {
//    return backward ? stepForward : stepBackward
//}

//var currentValue = 3
//let moveNearerToZezo = chooseStepFunction(backward: currentValue > 3)

// Nested functions
// define function inside the bodies of other function 
func otherChooseStepFunction (backward: Bool) -> (Int) -> Int {
    func stepForward(input: Int) -> Int {
        return input + 1
    }
    func stepBackward(input: Int) -> Int {
        return input - 1
    }
    return backward ? stepForward : stepBackward
}

var currentValue = 4
let moveNearerToZezo = otherChooseStepFunction(backward: currentValue > 0)


