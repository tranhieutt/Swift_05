//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//Pattern matching: Tuples, ranges and types
//Patten matching with tuples

//Check multiple data at one by grouping them in a tuple

let point = CGPoint (x: 7, y: 0)


switch (point.x, point.y) {
case (0,0):
    print("On the origin!")
case (0,_):
    print("x= 0: on Y - axis")
case (_,0):
    print("y=0: on X-axis")
case (let x, let y) where x==y:
    print("on y=x")
    
default:
    print("random point here")
}

//let x , let y la bind the variale to then be able to use a where

//Cases are evaluated in order

//String and Character: in swift, can also switch to a lot of native types, including string, character
let car: Character = "J"

switch car {
case "A", "B":
    print("Vowel")
default:
    <#code#>
}